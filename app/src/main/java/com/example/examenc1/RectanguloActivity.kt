package com.example.examenc1

import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity

class RectanguloActivity : AppCompatActivity() {
    private var numBase: EditText? = null
    private var numAltura: EditText? = null
    private var numArea: TextView? = null
    private var numPerimetro: TextView? = null
    private var btnCalcular: Button? = null
    private var btnLimpiar: Button? = null
    private var btnRegresar: Button? = null
    private var lblUsuario: TextView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rectangulo)
        iniciarComponentes()

        // Obtener los datos del MainActivity
        val datos = intent.extras
        val usuario = datos!!.getString("usuario")
        lblUsuario!!.text = usuario
        btnCalcular!!.setOnClickListener {
            if (validarCampos()) {
                calcular()
            } else {
                mostrarToast("Por favor, completa todos los campos")
            }
        }
        btnLimpiar!!.setOnClickListener { limpiarCampos() }
        btnRegresar!!.setOnClickListener { regresar() }
    }

    private fun iniciarComponentes() {
        numBase = findViewById<EditText>(R.id.numBase)
        numAltura = findViewById<EditText>(R.id.numAltura)
        numArea = findViewById<EditText>(R.id.numArea)
        numPerimetro = findViewById<EditText>(R.id.numPerimetro)
        btnCalcular = findViewById<Button>(R.id.btnCalcular)
        btnLimpiar = findViewById<Button>(R.id.btnLimpiar)
        btnRegresar = findViewById<Button>(R.id.btnRegresar)
        lblUsuario = findViewById<TextView>(R.id.lblUsuario)
    }

    private fun validarCampos(): Boolean {
        val numBaseVal = numBase!!.text.toString()
        val numAlturaVal = numAltura!!.text.toString()
        return !numBaseVal.isEmpty() && !numAlturaVal.isEmpty()
    }

    private fun calcular() {
        // Obtener los valores de entrada de los EditTexts
        val base = numBase!!.text.toString().toFloat()
        val altura = numAltura!!.text.toString().toFloat()

        val rectangulo = Rectangulo(base, altura)

        // Realizar los cálculos necesarios en el objeto reciboNomina
        val area= rectangulo.area()
        val perimetro = rectangulo.perimetro()

        // Mostrar los valores calculados en los TextViews
        numArea!!.text = area.toString()
        numPerimetro!!.text = perimetro.toString()
    }

    private fun limpiarCampos() {
        numBase!!.setText("")
        numAltura!!.setText("")
        numArea!!.text = ""
        numPerimetro!!.text = ""
    }

    private fun regresar() {
        val confirmar = AlertDialog.Builder(this)
        confirmar.setTitle("Rectangulo")
        confirmar.setMessage("Regresar al Menú Principal?")
        confirmar.setPositiveButton("Confirmar") { dialog, which -> finish() }
        confirmar.setNegativeButton("Cancelar") { dialog, which -> dialog.dismiss() }
        confirmar.show()
    }

    private fun mostrarToast(mensaje: String) {
        Toast.makeText(applicationContext, mensaje, Toast.LENGTH_SHORT).show()
    }
}