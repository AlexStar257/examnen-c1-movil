package com.example.examenc1

import java.text.DecimalFormat

class Rectangulo(// Getters y Setters
    var base: Float,
    var altura: Float,
) {

    fun area(): Float {
        val area = base * altura
        return formatFloat(area)
    }

    fun perimetro(): Float {
        val perimetro = (base + base) + (altura + altura)
        return formatFloat(perimetro)
    }

    private fun formatFloat(value: Float): Float {
        val decimalFormat = DecimalFormat("#.00")
        return decimalFormat.format(value.toDouble()).toFloat()
    }
}