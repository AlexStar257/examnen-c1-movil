package com.example.examenc1

import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog

class MainActivity : AppCompatActivity() {
    private var btnIngresar: Button? = null
    private var btnSalir: Button? = null
    private var txtUsuario: EditText? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        iniciarComponentes()
        btnIngresar!!.setOnClickListener { ingresar() }
        btnSalir!!.setOnClickListener { salir() }
    }

    private fun validarCampos(): Boolean {
        val usuarioVal = txtUsuario!!.text.toString()
        return !usuarioVal.isEmpty()
    }
//Relaciones
    private fun iniciarComponentes() {
        btnSalir = findViewById<Button>(R.id.btnSalir)
        btnIngresar = findViewById<Button>(R.id.btnIngresar)
        txtUsuario = findViewById<EditText>(R.id.txtUsuario)
    }
//Ingresar
    private fun ingresar() {
        val strUsuario = txtUsuario!!.text.toString()
        val bundle = Bundle()
        bundle.putString("usuario", strUsuario)
    if (validarCampos()) {

        val intent = Intent(this@MainActivity, RectanguloActivity::class.java)
        intent.putExtras(bundle)
        startActivity(intent)
    } else {
        mostrarToast("Por favor, completa todos los campos")
    }
    }
//Salir
    private fun salir() {
        val confirmar = AlertDialog.Builder(this)
        confirmar.setTitle("Rectangulo")
        confirmar.setMessage("¿Desea salir de la aplicación?")
        confirmar.setPositiveButton("Confirmar") { dialogInterface: DialogInterface?, which: Int -> finish() }
        confirmar.setNegativeButton("Cancelar") { dialogInterface: DialogInterface?, which: Int -> }
        confirmar.show()
    }
    private fun mostrarToast(mensaje: String) {
        Toast.makeText(applicationContext, mensaje, Toast.LENGTH_SHORT).show()
    }
}